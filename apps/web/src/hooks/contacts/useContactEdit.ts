import { useCallback } from 'react';
import { contactsClient } from '@lib/contactsClient';

export function useContactEdit() {
  const handleEditContact = useCallback(() => {
    const resp = await contactsClient.edit();
  }, []);

  return {
    handleEditContact,
  };
}
