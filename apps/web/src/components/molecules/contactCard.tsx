import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { InputInline } from '@components/molecules';
import { IContact } from 'react-coding-interview-shared/models';
import { useContactEdit } from '@hooks/contacts/useContactEdit';
import { useState } from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [isEditingName, setIsEditingName] = useState<boolean>(false);
  const [isEditingEmail, setIsEditingEmail] = useState<boolean>(false);

  const { handleEditContact } = useContactEdit();

  const onConfirmEditName = (value: string) => {
    handleEditContact(value);
  };

  const onConfirmEditEmail = (value: string) => {
    handleEditContact(value);
  };

  const onCancelEditName = () => {
    setIsEditingName(false);
  };

  const onCancelEditEmail = () => {
    setIsEditingEmail(false);
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <Typography variant="subtitle1" lineHeight="1rem">
            {isEditingName ? (
              <InputInline
                onCancel={onCancelEditName}
                onConfirm={onConfirmEditName}
                defaultValue={name}
              />
            ) : (
              name
            )}
          </Typography>
          <Typography variant="caption" color="text.secondary">
            {isEditingEmail ? (
              <InputInline
                onCancel={onCancelEditEmail}
                onConfirm={onConfirmEditEmail}
                defaultValue={email}
              />
            ) : (
              email
            )}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
