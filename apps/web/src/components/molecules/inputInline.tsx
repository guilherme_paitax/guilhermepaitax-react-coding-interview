import { Box, Button, TextField } from '@mui/material';
import { Close, Check } from '@mui/icons-material';
import { useState } from 'react';

export interface IInputInlineProps {
  label?: string;
  defaultValue?: string;
  onConfirm: (value: string) => void;
  onCancel: () => void;
  error?: boolean;
  errorMessage?: string;
}

export const InputInline: React.FC<IInputInlineProps> = ({
  label,
  onCancel,
  onConfirm,
  defaultValue = '',
  error,
  errorMessage,
}) => {
  const [value, setValue] = useState(defaultValue);

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <TextField
        error={error}
        value={value}
        label={label}
        helperText={errorMessage}
        onChange={(e) => setValue(e.target.value)}
      />
      <Box display="flex" alignItems="end">
        <Button onClick={() => onConfirm(value)}>
          <Check />
        </Button>
        <Button onClick={onCancel}>
          <Close />
        </Button>
      </Box>
    </Box>
  );
};
